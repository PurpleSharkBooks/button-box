A button box based on Arduino Leonardo, off the shelf components, and the [Joystick Library by MHeironimus](https://github.com/MHeironimus/ArduinoJoystickLibrary).

Also uses rotary.h from [here](https://github.com/brianlow/Rotary), based on [Rotary encoders, done properly ](http://www.buxtronix.net/2011/10/rotary-encoders-done-properly.html).