#include <Joystick.h>
#include <Rotary.h>
#include <Bounce2.h>

// Hardware setups and defines

// Rotary Encoders
#define RE_TOGGLE_TIME 50 // ms
// Rotary Encoder 1
#define RE1_DT 2
#define RE1_CLK 3
#define RE1_SW 4

// Rotary Encoder 2
#define RE2_DT 5
#define RE2_CLK 6
#define RE2_SW 7

// The larger toggles require a ton of debounce time to be usable
#define LARGE_TOGGLE_DEBOUNCE_TIME 250 // ms; first guess
#define LT1 8
#define LT2 9
#define LT3 10
#define LT4 11

// Small toggles
#define SMALL_TOGGLE_DEBOUNCE_TIME 10 // ms; first guess
#define ST1 12
#define ST2 13

// Mini push buttons
#define MINI_PUSH_DEBOUNCE_TIME 10 //ms ; first guess
#define PUSH1 A5
#define PUSH2 A4

// Total mappings: 14 out of 20 pins, only using 2 analog pins, leaving four available for axis inputs via ADC (potentiometers, for example)

Joystick_ Joystick(JOYSTICK_DEFAULT_REPORT_ID, JOYSTICK_TYPE_JOYSTICK,
                   14, 0, // buttons, hat switches
                   true, false, false, // x, y, z axis
                   false, false, false, // x, y, z rotational axis
                   false, false, // rudder, throttle
                   false, false, false); // accelerator, brake, steering

// Configure rotary encoders, this one uses pins 6 and 7
Rotary rotary1 = Rotary(RE1_CLK, RE1_DT); // CLK, DT on KY-040 rotary encoder
Rotary rotary2 = Rotary(RE2_CLK, RE2_DT); // ditto

Bounce st1 = Bounce();
Bounce st2 = Bounce();

Bounce pb1 = Bounce();

// Joystick button mappings and initial states
// Digital (i.e. on/off) inputs
const int re_1_button  = 2;
const int re_2_button  = 5;
int lastRE1ButtonState = 0;
int lastRE2ButtonState = 0;

// Analog (i.e. range of values) inputs
// const int axisPot = A0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // for debugging via the Arduino IDE serial console; note that USB doesn't have a baud rate as such and this is just the general bottom value of ye olde serial hardware
  rotary1.begin();
  rotary2.begin();

  // Initialize Button Pins
  pinMode(RE1_SW, INPUT);
  pinMode(RE2_SW, INPUT);

  st1.attach(ST1, INPUT);
  st1.interval(SMALL_TOGGLE_DEBOUNCE_TIME);
  st2.attach(ST2, INPUT);
  st2.interval(SMALL_TOGGLE_DEBOUNCE_TIME);

  pb1.attach(PUSH1, INPUT);
  pb1.interval(MINI_PUSH_DEBOUNCE_TIME);
  // Initialize and configure Joystick library
  // Joystick.setRudderRange(-512, 511); // center the Rudder axis to get it moving off center in both negative and positive directions
  Joystick.begin();
}

void loop() {
  unsigned char result1 = rotary1.process();
  if (result1 == DIR_CW)
  {
    Joystick.setButton(1, 1);
    delay(RE_TOGGLE_TIME);
    Joystick.setButton(1, 0);
  } else if (result1 == DIR_CCW)
  {
    Joystick.setButton(0, 1);
    delay(RE_TOGGLE_TIME);
    Joystick.setButton(0, 0);
  }
  int currentRE1ButtonState = !digitalRead(RE1_SW); // 10kOhm pullup resistor, which means read values are inverted from what we want; invert for toggle switches
  if (currentRE1ButtonState != lastRE1ButtonState)
  {
    Joystick.setButton(re_1_button, currentRE1ButtonState);
    lastRE1ButtonState = currentRE1ButtonState;
  }
  unsigned char result2 = rotary2.process();
  if (result2 == DIR_CW)
  {
    Joystick.setButton(4, 1);
    delay(RE_TOGGLE_TIME);
    Joystick.setButton(4, 0);
  } else if (result2 == DIR_CCW)
  {
    Joystick.setButton(3, 1);
    delay(RE_TOGGLE_TIME);
    Joystick.setButton(3, 0);
  }
  int currentRE2ButtonState = !digitalRead(RE2_SW); // 10kOhm pullup resistor, which means read values are inverted from what we want; invert for toggle switches
  if (currentRE2ButtonState != lastRE2ButtonState)
  {
    Joystick.setButton(re_2_button, currentRE2ButtonState);
    lastRE2ButtonState = currentRE2ButtonState;
  }
  st1.update();
  if ( st1.changed() )
  {
    // THE STATE OF THE INPUT CHANGED
    int debouncedValueST1 = st1.read();
    // DO SOMETHING WITH THE VALUE
    Joystick.setButton(6, !debouncedValueST1);
    Serial.print("toggled 1: ");
    Serial.println(debouncedValueST1);
  }
  st2.update();
  if ( st2.changed() )
  {
    // THE STATE OF THE INPUT CHANGED
    int debouncedValueST2 = st2.read();
    // DO SOMETHING WITH THE VALUE
    Joystick.setButton(7, !debouncedValueST2);
    Serial.print("toggled 2: ");
    Serial.println(debouncedValueST2);
  }
  pb1.update();
  if ( pb1.changed() )
  {
    // THE STATE OF THE INPUT CHANGED
    int debouncedValuepb1 = pb1.read();
    // DO SOMETHING WITH THE VALUE
    Joystick.setButton(8, debouncedValuepb1);
    Serial.print("push1: ");
    Serial.println(debouncedValuepb1);
  }
}
